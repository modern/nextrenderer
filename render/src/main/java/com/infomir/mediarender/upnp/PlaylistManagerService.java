package com.infomir.mediarender.upnp;

import android.content.ContentResolver;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import com.infomir.mediarender.InfoRender;

import org.teleal.cling.binding.annotations.UpnpAction;
import org.teleal.cling.binding.annotations.UpnpInputArgument;
import org.teleal.cling.binding.annotations.UpnpOutputArgument;
import org.teleal.cling.binding.annotations.UpnpService;
import org.teleal.cling.binding.annotations.UpnpServiceId;
import org.teleal.cling.binding.annotations.UpnpServiceType;
import org.teleal.cling.binding.annotations.UpnpStateVariable;
import org.teleal.cling.binding.annotations.UpnpStateVariables;
import org.teleal.cling.model.ModelUtil;
import org.teleal.cling.model.types.UnsignedIntegerFourBytes;
import org.teleal.cling.support.avtransport.AVTransportErrorCode;
import org.teleal.cling.support.avtransport.AVTransportException;
import org.teleal.cling.support.model.MediaInfo;
import org.teleal.cling.support.model.PositionInfo;
import org.teleal.cling.support.model.SeekMode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@UpnpService(
        serviceId = @UpnpServiceId(namespace = "jinzora.org", value = "PlaylistManager"),
        serviceType = @UpnpServiceType(namespace = "jinzora.org", value = "PlaylistManager", version = 1)
)

@UpnpStateVariables({
        @UpnpStateVariable(
                name = "AVTransportURI",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "AVTransportURIMetaData",
                sendEvents = false,
                datatype = "string",
                defaultValue = "NOT_IMPLEMENTED"),

        @UpnpStateVariable(
                name = "A_ARG_TYPE_InstanceID", // префикс A_ARG_TYPE - state variables which are only ever used in action arguments
                sendEvents = false,
                datatype = "ui4"),
        // TODO dev
        @UpnpStateVariable(
                name = "Track",
                sendEvents = false,
                datatype = "ui4",
                defaultValue = "0"),

        @UpnpStateVariable(
                name = "TrackDuration",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "TrackMetaData",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "TrackURI",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "RelTime",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "AbsTime",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "RelCount",
                sendEvents = false,
                datatype = "i4",
                defaultValue = "2147483647"),

        @UpnpStateVariable(
                name = "AbsCount",
                sendEvents = false,
                datatype = "i4",
                defaultValue = "2147483647"),

        // ################
        @UpnpStateVariable(
                name = "Unit",
                sendEvents = false,
                datatype = "string"),

        @UpnpStateVariable(
                name = "Target",
                sendEvents = false,
                datatype = "string"),

        // TODO dev
})

public class PlaylistManagerService {
    public static final String UNKNOWN_TRACK = "Unknown Track";
    private static final String TAG = "PlaylistManagerService";
    public static final String META_PLAYLIST_CHANGED = "NOTIFY_AVTRANSPORT_SERVICE";
    private static UnsignedIntegerFourBytes sInstanceId = new UnsignedIntegerFourBytes(0);
    private MediaRenderer mMediaRenderer;
    private MediaInfo currentMediaInfo = new MediaInfo(); // TODO dev
    private PositionInfo currentPositionInfo = new PositionInfo(); // TODO dev

    public PlaylistManagerService(MediaRenderer mediaRenderer) {
        mMediaRenderer = mediaRenderer;
    }

    public static UnsignedIntegerFourBytes getPlayerInstanceId() {
        return sInstanceId;
    }

    // Local actions
    public void doPlaylist(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        doPlaylist(urlConnection.getInputStream());
    }

    public void doPlaylist(ContentResolver resolver, Uri playlistUri) throws IOException {
        InputStream playlistInput = resolver.openInputStream(playlistUri);
        doPlaylist(playlistInput);
    }

    public void doPlaylist(InputStream playlistInput) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(playlistInput));
        String line = null;
        String lastLine = null;
        boolean trackAdded = false;
        line = br.readLine();
        while (line != null) {
            if (line.length() > 0 && line.charAt(0) != '#') {
                try {
                    URL track = new URL(line);
                    String trackname;

                    if (lastLine.charAt(0) == '#') {
                        int pos;
                        if (-1 != (pos = lastLine.indexOf(','))) {
                            trackname = lastLine.substring(pos + 1, lastLine.length());
                        } else {
                            trackname = UNKNOWN_TRACK;
                        }
                    } else {
                        trackname = UNKNOWN_TRACK;
                    }

                    try {
                        setAVTransportURI(sInstanceId, track.toString(), null);
                        getPlaylist().add(track.toString(), null);
                        trackAdded = true;
                    } catch (Exception e) {
                        Log.e(TAG, "Error playing track", e);
                    }
                } catch (Exception e) {
                    // probably a comment line
                }
            }

            lastLine = line;
            line = br.readLine();
        }
        /*
        if (trackAdded) {
			try {
				// TODO: only play if we are stopped / paused.
				mMediaRenderer.getAVTransportService().play(MediaRenderer.getPlayerInstanceId(), null);
			} catch (AVTransportException e) {
				Log.e(TAG, "Error playing media", e);
			}
		}
        */
    }

    @UpnpAction // TODO dev
    public void seek(
            //@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes arg0,
            @UpnpInputArgument(name = "Unit", stateVariable = "A_ARG_TYPE_SeekMode") String arg1,
            @UpnpInputArgument(name = "Target", stateVariable = "A_ARG_TYPE_SeekTarget") String arg2)
            throws AVTransportException {
        // TODO Auto-generated method stub

        // final DefMediaPlayer player = getInstance(arg0); arg0 ???

        MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
        SeekMode seekMode;
        try {
            seekMode = SeekMode.valueOrExceptionOf(arg1);

            if (!seekMode.equals(SeekMode.REL_TIME)) {
                throw new IllegalArgumentException();
            }

            /*final ClockTime ct = ClockTime.fromSeconds(ModelUtil.fromTimeString(arg2));
            if (player.getPipeline().getState().equals(State.PLAYING)) {
                player.pause();
                player.getPipeline().seek(ct);
                player.play();
            } else if (player.getPipeline().getState().equals(State.PAUSED)) {
                player.getPipeline().seek(ct);
            }*/

            // arg2 is in format of "hh:mm:ss"
            Log.d(TAG,"seek target = " + arg2);
            Log.d(TAG,"seek target = " + ModelUtil.fromTimeString(arg2));
            player.seekTo(((Long)ModelUtil.fromTimeString(arg2)).intValue() * 1000);
            /*if (player.isPlaying()){
            	player.pause();
            	player.seekTo(Integer.parseInt(arg2));
            	player.play();
            } else {
            	player.seekTo(Integer.parseInt(arg2));
            }*/

        } catch (IllegalArgumentException ex) {
            throw new AVTransportException(
                    AVTransportErrorCode.SEEKMODE_NOT_SUPPORTED, "Unsupported seek mode: " + arg1
            );
        }
    }

    @UpnpAction
    public void clear(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes instanceId) {
        Log.d(TAG, "playlistManager::clear called");
        try {
            if (mPlaylists.containsKey(instanceId)) {
                mPlaylists.get(instanceId).clear();
                MediaRenderer.getInstance().getAVTransportService()
                        .stop(instanceId);
            }
        } catch (AVTransportException e) {
            Log.w(TAG, "Error stopping player", e);
        }
    }

    @UpnpAction
    public void setAVTransportURI(
            @UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes instanceId,
            @UpnpInputArgument(name = "CurrentURI", stateVariable = "AVTransportURI") String currentURI,
            @UpnpInputArgument(name = "CurrentURIMetaData", stateVariable = "AVTransportURIMetaData") String currentURIMetaData) {

        Log.d(TAG, "Called setURI with " + currentURI);

        InfoRender.intentionTransfer(currentURI); // TODO

        if (!mPlaylists.containsKey(instanceId)) {
            mPlaylists.put(instanceId, new Playlist());
        }
        mPlaylists.get(instanceId).add(currentURI, currentURIMetaData);

        // Notify the AVTransportService in case this alters our machine state.
        try {
            MediaRenderer.getInstance().getAVTransportService()
                    .setAVTransportURI(instanceId, currentURI, META_PLAYLIST_CHANGED);
        } catch (AVTransportException e) {
            Log.e(TAG, "Error notifying of playlist update", e);
        }

        try {
            Log.e(TAG, "" + MediaRenderer.getInstance().getAVTransportService().getPositionInfo(instanceId).getTrackDuration());
            //Log.e(TAG, "" + MediaRenderer.getInstance().getAVTransportService().seek(););
        } catch (AVTransportException e) {
            e.printStackTrace();
        }
    }

    /* // TODO dev список дефолтовых возможностей
    @UpnpAction
    public MediaInfo getMediaInfo(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {
        return null;
    }

    @UpnpAction
    public TransportInfo getTransportInfo(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {
        return null;
    }

    @UpnpAction
    public PositionInfo getPositionInfo(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {
        return null;
    }

    @UpnpAction
    public DeviceCapabilities getDeviceCapabilities(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {
        return null;
    }

    @UpnpAction
    public TransportSettings getTransportSettings(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {
        return null;
    }

    @UpnpAction
    public void stop(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {

    }

    @UpnpAction
    public void play(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes, @UpnpInputArgument(name = "Speed", stateVariable = "TransportPlaySpeed") String s) throws AVTransportException {

    }

    @UpnpAction
    public void pause(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {

    }

    @UpnpAction
    public void record(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {

    }

    @UpnpAction
    public void seek(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes, @UpnpInputArgument(name = "Unit", stateVariable = "A_ARG_TYPE_SeekMode") String s, @UpnpInputArgument(name = "Target", stateVariable = "A_ARG_TYPE_SeekTarget") String s2) throws AVTransportException {

    }

    @UpnpAction
    public void next(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {

    }

    @UpnpAction
    public void previous(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {

    }

    @UpnpAction
    public void setPlayMode(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes, @UpnpInputArgument(name = "NewPlayMode", stateVariable = "CurrentPlayMode") String s) throws AVTransportException {

    }

    @UpnpAction
    public void setRecordQualityMode(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes, @UpnpInputArgument(name = "NewRecordQualityMode", stateVariable = "CurrentRecordQualityMode") String s) throws AVTransportException {

    }

    @UpnpAction
    public String getCurrentTransportActions(@UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes unsignedIntegerFourBytes) throws AVTransportException {
        return null;
    }
    */

    // TODO dev
    protected MediaPlayer getInstance() throws AVTransportException {
        MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
        if (player == null) {
            throw new AVTransportException(AVTransportErrorCode.INVALID_INSTANCE_ID);
        }
        return player;
    }
    // TODO dev
    synchronized public PositionInfo getCurrentPositionInfo() {

        MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
        int i = 0; // TODO dev удалить ! для теста
        currentPositionInfo =
                new PositionInfo(
                        1,
                        currentMediaInfo.getMediaDuration(),
                        currentMediaInfo.getCurrentURI(),
                        ModelUtil.toTimeString(player.getCurrentPosition() / 1000) + i++,
                        ModelUtil.toTimeString(player.getCurrentPosition() / 1000) + i++ // TODO i++ удалить, для теста
                );

        return currentPositionInfo;
    }

    // TODO dev
    @UpnpAction(out = {
            @UpnpOutputArgument(name = "Track", stateVariable = "CurrentTrack", getterName = "getTrack"),
            @UpnpOutputArgument(name = "TrackDuration", stateVariable = "CurrentTrackDuration", getterName = "getTrackDuration"),
            @UpnpOutputArgument(name = "TrackMetaData", stateVariable = "CurrentTrackMetaData", getterName = "getTrackMetaData"),
            @UpnpOutputArgument(name = "TrackURI", stateVariable = "CurrentTrackURI", getterName = "getTrackURI"),
            @UpnpOutputArgument(name = "RelTime", stateVariable = "RelativeTimePosition", getterName = "getRelTime"),
            @UpnpOutputArgument(name = "AbsTime", stateVariable = "AbsoluteTimePosition", getterName = "getAbsTime"),
            @UpnpOutputArgument(name = "RelCount", stateVariable = "RelativeCounterPosition", getterName = "getRelCount"),
            @UpnpOutputArgument(name = "AbsCount", stateVariable = "AbsoluteCounterPosition", getterName = "getAbsCount")})
    public PositionInfo getPositionInfo(
            @UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes instanceId)
            throws AVTransportException {
        Log.d(TAG, "getPositionInfo called");

        return getCurrentPositionInfo();//getInstance().getCurrentPositionInfo();
    }

    /*
    //TODO dev добавляю прокрутку
    @UpnpAction
    public void seek(
            @UpnpInputArgument(name = "InstanceID") UnsignedIntegerFourBytes arg0,
            @UpnpInputArgument(name = "Unit", stateVariable = "A_ARG_TYPE_SeekMode") String arg1,
            @UpnpInputArgument(name = "Target", stateVariable = "A_ARG_TYPE_SeekTarget") String arg2)
            throws AVTransportException {
        // TODO Auto-generated method stub

        final MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
        //final DefMediaPlayer player = getInstance(arg0); // оригинал из китайца

        SeekMode seekMode;
        try {
            seekMode = SeekMode.valueOrExceptionOf(arg1);

            if (!seekMode.equals(SeekMode.REL_TIME)) {
                throw new IllegalArgumentException();
            }

            // arg2 is in format of "hh:mm:ss"
            Log.d(TAG,"seek target = " + arg2);
            Log.d(TAG,"seek target = " + ModelUtil.fromTimeString(arg2));
            player.seekTo(((Long)ModelUtil.fromTimeString(arg2)).intValue() * 1000);

        } catch (IllegalArgumentException ex) {
            throw new AVTransportException(
                    AVTransportErrorCode.SEEKMODE_NOT_SUPPORTED, "Unsupported seek mode: " + arg1
            );
        }
    }
    */

    public void setCursor(UnsignedIntegerFourBytes instanceId, int position) {
        Playlist p = mPlaylists.get(instanceId);
        if (p == null) {
            return;
        }
        p.cursor = position;
    }

    public void advanceCursor(UnsignedIntegerFourBytes instanceId) {
        Playlist p = mPlaylists.get(instanceId);
        if (p == null) {
            return;
        }
        p.cursor++;
    }

    public void jumpTo(UnsignedIntegerFourBytes instanceId, int position) {
        Playlist p = mPlaylists.get(instanceId);
        if (p == null) {
            return;
        }
        Log.d(TAG, "jumping to " + position);
        p.cursor = position;
    }

    public int getLength(UnsignedIntegerFourBytes instanceId) {
        Playlist p = mPlaylists.get(instanceId);
        if (p == null) {
            return -1;
        }
        return p.list.size();
    }

    // next/prev/play/pause managed by AVTransport.

    // add:
    // queueToEnd, queueNext, replaceWith(?)
    // support single items, playlists, and multiple items.

    public Playlist getPlaylist() {
        return mPlaylists.get(getPlayerInstanceId());
    }

    Map<UnsignedIntegerFourBytes, Playlist> mPlaylists = new ConcurrentHashMap<UnsignedIntegerFourBytes, Playlist>();

    public class Playlist {
        public static final int FIRST_ENTRY = 0;
        public List<PlaylistEntry> list;
        public int cursor;

        public Playlist() {
            list = new ArrayList<PlaylistEntry>();
            cursor = FIRST_ENTRY;
        }

        public void add(String uri, String metadata) {
            // TODO: if playlist is an m3u, get its entries.
            PlaylistEntry e = new PlaylistEntry(uri, metadata);
            list.add(e);
        }

        public void clear() {
            list.clear();
        }
    }

    public class PlaylistEntry {
        public String uri;
        public String metadata;

        public PlaylistEntry(String uri, String metadata) {
            this.uri = uri;
            this.metadata = metadata;
        }
    }
}
