package com.infomir.mediarender.upnp;

import com.infomir.mediarender.upnp.statemachine.PBNoMediaPresent;
import com.infomir.mediarender.upnp.statemachine.PBPaused;
import com.infomir.mediarender.upnp.statemachine.PBPlaying;
import com.infomir.mediarender.upnp.statemachine.PBStopped;

import org.teleal.cling.support.avtransport.impl.AVTransportStateMachine;
import org.teleal.common.statemachine.States;

@States({
        PBNoMediaPresent.class,
        PBStopped.class,
        PBPlaying.class,
        PBPaused.class
})
interface PBRendererStateMachine extends AVTransportStateMachine {}