package com.infomir.mediarender.upnp.statemachine;

import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import com.infomir.mediarender.InfoRender;
import com.infomir.mediarender.upnp.MediaRenderer;
import com.infomir.mediarender.upnp.PlaylistManagerService;

import org.teleal.cling.support.avtransport.impl.state.AbstractState;
import org.teleal.cling.support.avtransport.impl.state.PausedPlay;
import org.teleal.cling.support.model.AVTransport;
import org.teleal.cling.support.model.PositionInfo;
import org.teleal.cling.support.model.SeekMode;

import java.net.URI;

public class PBPaused extends PausedPlay<AVTransport> {
	private static final String TAG = "PBPaused";

    private static String ACTION = "com.infomir.mediarender.PLAYER"; // Broadcast Receiver
    private static final String TYPE = "action";
    private static final int ID_ACTION_PLAY = 1;

    URI uri;
    String metaData;
	
    public PBPaused(AVTransport transport) {
        super(transport);
    }

    @Override
    public void onEntry() {
        super.onEntry();

        Log.d(TAG, "pausing track");
        MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
    	if (player.isPlaying()) {
    		player.pause();
    	}
    }

    @Override
    public Class<? extends AbstractState> setTransportURI(URI uri, String metaData) {
    	Log.d(TAG, "called Playing::setTransportURI with " + uri);

        this.uri = uri;
        this.metaData = metaData;

    	if (!PlaylistManagerService.META_PLAYLIST_CHANGED.equals(metaData)) {
    		PlaylistManagerService pmService = MediaRenderer.getInstance().getPlaylistManager();
    		pmService.setAVTransportURI(getTransport().getInstanceId(), uri.toString(), metaData);
    	}
    	
        return PBPaused.class;
    }

    @Override
    public Class<? extends AbstractState> stop() {
        // Stop playing!
    	Log.d(TAG, "Playing::stop called");
    	MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
    	if (player.isPlaying()) {
    		player.stop();
    	}
        return PBStopped.class;
    }

	public Class<? extends AbstractState> next() {
		Log.d(TAG, "Paused::next called");
		return PBTransitionHelpers.next(this, PBPaused.class);
	}

	public Class<? extends AbstractState> pause() {
		Log.d(TAG, "Paused::pause called");
		return null;
	}

	@Override
	public Class<? extends AbstractState> play(String speed) {
		Log.d(TAG, "Paused::play called");
		MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
    	if (!player.isPlaying()) {
    		//player.start(); // todo закомменть
    	}

        Log.d(TAG,"=== перед вызовом sendBroadcast ===");

        Intent intent = new Intent(ACTION);
        intent.putExtra(TYPE, ID_ACTION_PLAY);
        InfoRender.mcontext.sendBroadcast(intent);

        AVTransport transport = getTransport();
        PositionInfo positionInfo = new PositionInfo();

        Log.d(TAG, "" + transport.getPositionInfo().getAbsTime());

        //this.seek(SeekMode.REL_TIME);

        //seek(SeekMode.REL_TIME, "00:01:00");

        //12-18 12:47:52.406    2819-2847/com.infomir.mediarender E/MediaPlayer﹕ Attempt to perform seekTo in wrong state: mPlayer=0x1e1b28, mCurrentState=2
        //12-18 12:47:52.406    2819-2847/com.infomir.mediarender E/MediaPlayer﹕ error (-38, 0)
        //12-18 12:47:52.406    2819-2819/com.infomir.mediarender E/MediaPlayer﹕ Error (-38,0)

        //transport.getPositionInfo().getAbsTime();

        /*
        UnsignedIntegerFourBytes instanceId = getTransport().getInstanceId();
        PlaylistManagerService pmService = MediaRenderer.getInstance().getPlaylistManager();
        pmService.setAVTransportURI(instanceId, uri.toString(), metaData);
        int position = pmService.getLength(instanceId) - 1;
        pmService.jumpTo(instanceId, position);
        */

        //PlaylistManagerService pmService = MediaRenderer.getInstance().getPlaylistManager();
        //pmService.setCursor(MediaRenderer.getPlayerInstanceId(), 0);
        //pmService.jumpTo(MediaRenderer.getPlayerInstanceId(), 30);

        /*
        getTransport().setPositionInfo(
                new PositionInfo(1,"00:03:00", uri.toString(), "00:01:00", "00:01:00")
        ); */

		return PBPlaying.class;
	}

	public Class<? extends AbstractState> previous() {
		Log.d(TAG, "Paused::prev called");
		return null;
	}

	public Class<? extends AbstractState> seek(SeekMode unit, String target) {
		if (unit.equals(SeekMode.REL_TIME)) {
			MediaPlayer player = MediaRenderer.getInstance().getMediaPlayer();
			player.seekTo(PBTransitionHelpers.timeInMS(target));
		}
		return null;
	}
}